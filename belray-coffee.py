#!/usr/bin/python
# -*- coding: UTF-8 -*-

# 贝瑞咖啡

import requests
import sys
import os

env = os.getenv("ENV")
if env == None or len(env) == 0:
    print("环境名为空")
    sys.exit()
elif "BELRAY-COFFEE" != env:
    print("非贝瑞咖啡脚本")
    sys.exit()

api = "https://appapi.belray-coffee.com/belray/api/user/userSign"

phone = os.getenv("PHONE")

if phone == None or len(phone) == 0:
    print("phone 为空")
    sys.exit()

auth = os.getenv("AUTH")
data = "phone=" + phone

headers = {
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "Authorization": auth
}

r = requests.post(api, data=data, headers=headers)
print(r.text)